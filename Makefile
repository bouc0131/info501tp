$(CC) = gcc
CFLAGS = -Wall -Wextra -pedantic -std=c11
OBJETS = build/main.o build/cellule.o build/liste.o build/graphe.o


main.out:build/main.o build/cellule.o build/liste.o build/graphe.o
	$(CC) $(CFLAGS) -o main.out $(OBJETS)

build/main.o:src/main.c
	$(CC) $(CFLAGS) -c src/main.c -o build/main.o

build/graphe.o:src/graphe.c
	$(CC) $(CFLAGS) -c src/graphe.c -o build/graphe.o

build/liste.o:src/liste.c
	$(CC) $(CFLAGS) -c src/liste.c -o build/liste.o

build/cellule.o:src/cellule.c
	$(CC) $(CFLAGS) -c src/cellule.c -o build/cellule.o

clean:
	rm $(OBJETS) main.out