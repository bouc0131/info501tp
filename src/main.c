#include <stdio.h>
#include <stdlib.h>

#include "cellule.h"
#include "liste.h"
#include "graphe.h"

int main()
{
    graphe g;
    initialiser_graphe(&g, "graphe.txt");
    afficher_graphe(&g);
    detruire_graphe(&g);
}
