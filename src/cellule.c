#include <malloc.h>
#include <printf.h>

#include "cellule.h"

void initialiser_cellule(cellule *c, int id_sommet)
{
    c->pred = 0;
    c->succ = 0;
    c->id_sommet = id_sommet;
}

void afficher_cellule(cellule const *c)
{
    printf("Cellule n°%d pred:%p  succ:%p\n", c->id_sommet, (void *)c->pred, (void *)c->succ);
}

cellule *allouer_cellule(int id_sommet)
{
    cellule *c = (cellule *)malloc(sizeof(cellule));
    initialiser_cellule(c, id_sommet);

    return c;
}