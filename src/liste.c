#include <printf.h>
#include <malloc.h>

#include "liste.h"
#include "cellule.h"

void initialiser_liste(liste *l)
{
    l->tete = (cellule *)0;
}

void inserer_liste(liste *l, cellule *c)
{
    c->succ = l->tete;
    c->pred = (cellule *)0;
    l->tete = c;

    if (c->succ)
    {
        c->succ->pred = c;
    }
}

void afficher_liste(liste *l)
{
    printf("tete ");

    if (l->tete == 0)
    {
        printf("/\n");
        return;
    }

    cellule *temp = l->tete;
    printf("--> ");

    while (temp)
    {
        printf("%d ", temp->id_sommet);
        temp = temp->succ;
    }

    printf("\n");
}

void supprimer_liste(liste *l, cellule *c)
{
    if (c->succ)
    {
        c->succ->pred = c->pred;
    }
    if (c->pred)
    {
        c->pred->succ = c->succ;
    }
    else
    {
        l->tete = c->succ;
    }

    free(c);
}

void detruire_liste(liste *l)
{
    cellule *c = l->tete;
    while (c)
    {
        cellule *temp = c->succ;
        free(c);
        c = temp;
    }
}

liste *allouer_liste()
{
    liste *l = (liste *)malloc(sizeof(liste));
    initialiser_liste(l);

    return l;
}

cellule *rechercher_liste(liste *l, int id_sommet)
{
    cellule *temp = l->tete;

    while (temp && temp->id_sommet != id_sommet)
    {
        temp = temp->succ;
    }

    return temp;
}