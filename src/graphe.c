#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "graphe.h"
#include "stdbool.h"
#include "liste.h"

void ajouter_arete(graphe *g, int idx, int idy)
{
    if (g == NULL)
    {
        printf("graphe pass is null\n");
        exit(2);
    }
    inserer_liste(g->l_adj + idx, allouer_cellule(idy));
    g->m_adj[idx][idy] = 1;
    if ((!g->oriente))
    {
        g->m_adj[idy][idx] = 1;
        inserer_liste(g->l_adj + idy, allouer_cellule(idx));
    }
}

void no_name(FILE *f, graphe *g);

void initialiser_graphe(graphe *g, const char *path)
{
    FILE *file = fopen(path, "r");
    if (file == NULL)
    {
        perror("can't open the file!");
        exit(1);
    }
    char buffer[50];
    int n_sommets = 0;
    int oriente = 0;
    int value = 0;
    int input;

    for (int i = 0; i < 3;)
    {
        // printf("        read:%d buffer: %s input: %d\n", ret, buffer, input); debug
        if (fscanf(file, "%49[^ ] %d\n", buffer, &input) != 2)
        {
            printf("Fichier mal formaté\n");
            exit(1);
        }
        if (strcmp("n_sommets", buffer) == 0)
        {
            n_sommets = input;
            i++;
            continue;
        }
        if (strcmp("oriente", buffer) == 0)
        {
            oriente = input;
            i++;
            continue;
        }
        if (strcmp("value", buffer) == 0)
        {
            value = input;
            i++;
            continue;
        }
        printf("Mauvais formatage: buffer: %s, input: %d\n", buffer, input);
        exit(1);
    }

    g->n_sommets = n_sommets;
    g->oriente = oriente;
    g->value = value;

    g->l_adj = (liste *)malloc(sizeof(liste) * n_sommets);
    g->m_adj = (int **)malloc(n_sommets * sizeof(int *));
    g->m_stockage = (int *)malloc(n_sommets * n_sommets * sizeof(int));

    for (int i = 0; i < n_sommets; i++)
    {
        g->m_adj[i] = g->m_stockage + i * n_sommets;
    }
    for (int i = 0; i < n_sommets; i++)
    {
        initialiser_liste(g->l_adj + i);
    }

    while (fgets(buffer, sizeof(buffer), file))
    {
        if (strcmp("DEBUT_DEF_ARETES\n", buffer) == 0)
        {
            no_name(file, g);
            break;
        }
    }
    fclose(file);
}
void no_name(FILE *f, graphe *g)
{
    int x, y;
    while (fscanf(f, "%d %d\n", &x, &y) == 2)
    {
        // printf("ajout de l'arete: %d %d\n", x, y);
        if (y < 0 || x < 0 || (long unsigned int)x >= g->n_sommets || (long unsigned int)y >= g->n_sommets)
        {
            break;
        }
        ajouter_arete(g, x, y);
    }
}

void afficher_graphe(const graphe *g)
{
    printf("\nNombre de sommets: %ld\nLe graphe %s valeurs\nIl %s orienté\n\n", g->n_sommets, g->value ? "possède des" : "ne possède pas de", g->oriente ? "est" : "n'est pas");
    for (size_t i = 0; i < g->n_sommets; i++)
    {
        for (size_t j = 0; j < g->n_sommets; j++)
        {
            printf("%c  ", g->m_adj[i][j] ? 'X' : ' ');
        }
        putchar('\n');
    }
    printf("\n\n");
    for (size_t i = 0; i < g->n_sommets; i++)
    {
        printf("%lu  ", i);
        afficher_liste(g->l_adj + i);
    }
}

void detruire_graphe(graphe *g)
{
    free(g->m_stockage);
    for (size_t i = 0; i < g->n_sommets; i++)
    {
        detruire_liste(g->l_adj + i);
    }
    free(g->m_adj);
    free(g->l_adj);
}