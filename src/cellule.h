#ifndef CELLULE
#define CELLULE

typedef struct cellule_type
{
    int id_sommet;
    struct cellule_type *pred;
    struct cellule_type *succ;
} cellule;

void initialiser_cellule(cellule *c, int id_sommet);
cellule *allouer_cellule(int id_sommet);
void afficher_cellule(cellule const *c);

#endif